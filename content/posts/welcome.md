+++
title = "Welcome"
description = "Introducing myself and my website."
date = 2020-09-28
[taxonomies]
categories = ["Introduction"]
tags = ["about me", "personal"]

[extra]
comments = false
+++

Welcome to [natpbs.it]. I am Nathan Seddig and this website was created with twofold purpose: it is meant both as a homepage to showcase personal information such as my contacts and my resume and as a place where to publish some work of mine, of either written or graphical kind.

I hope that you'll find what you're looking for!

— Nathan, *28th September 2020*

[natpbs.it]: https://natpbs.it "natpbs.it"